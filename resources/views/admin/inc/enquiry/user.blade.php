<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    @include('admin.common.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        @include('admin.common.TopHeader')
        <!-- Begin Page Content -->
        <div class="container-fluid">
        {!! Form::open(['method' => 'GET']) !!}
                <div class="row">
                  <div class="col-lg-5">  
                  </div>
                  <div class="col-lg-3"> 
                    <!-- @php $abc = request('date') ?? date('Y-m-d'); @endphp -->
                    <div class="form-group">
                      <!-- <input type="date" name="date" class="form-control" id="datepicker" value="{{ $abc }}"> -->
                      <input type="date" name="date" class="form-control" id="datepicker" value="{{request('date')}}">
                    </div>  
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                    
                      @php
                      $user = App\User::where('role_id',2)->get();
                      $userArr = [
                      '' => 'Select User'
                      ];
                      foreach ($user as $mcat) {
                            $userArr[$mcat->id] = $mcat->name;
                        }
                    @endphp
                    
                    
                    {{Form::select('user', $userArr, request('user'), ['class' => 'form-control ','id'=>'user'])}}
                     
                    </div>    
                  </div>

                  <div class="col-lg-1">
                      <button type="submit" class="btn btn-danger btn-md m-0" style="margin:0">Filter</button>
                  </div>
                  
                </div>
                {!! Form::close() !!}
          {{ Form::open(['url' => url(env('ADMIN_DIR').'/enquiry/user')]) }}
          <!-- Page Heading -->
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="card">
                <div class="card-header d-sm-flex align-items-center justify-content-between mb-4">
                  <h6 class="m-0 font-weight-bold text-primary">User Enquiry List</h6>
                  <div class="">
                    <!-- <a href="{{ route('marchant.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">+ Add Marchant</a> -->
                    <!-- <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-url="{{ url(env('ADMIN_DIR').'/marchant/delete') }}" id="delete_all">Delete</button> -->

                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                      <tr>
                        <th>S No.</th>
                        <th>Name</th>
                        <th>mobile</th>
                        <th>email</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Shop</th>
                        <th>Category</th>
                        <th>Date</th>

                        <!-- <th>Action</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($lists as $key => $list)
                      <tr class="bg-light">
                        <td>{{ $key+1 }}. 
                          <!-- <input type="checkbox" name="sub_chk[]" value="{{ $list->id }}" class="sub_chk" data-id="{{$list->id}}"> -->
                        </td>
                        <td>
                          <!-- <a href="{{route('marchant.edit', $list->id) }}">
                            <i class="far fa-edit" aria-hidden="true"></i>  -->
                            {{$list->enquery ? $list->enquery->name : 'N/A'}}
                          <!-- </a> -->
                        </td>
                        <td>{{$list->enquery ? $list->enquery->mobile : 'N/A'}}</td>
                        <td>{{$list->enquery ? $list->enquery->email : 'N/A'}}</td>
                        <td>{{$list->name ?? 'N/A'}}</td>
                        <td>{{$list->price ?? 'N/A'}}</td>
                        <td>{{$list->enquery->shop ? $list->enquery->shop->name : 'N/A'}}</td>
                        <td>{{$list->category ? $list->category->name : 'N/A'}}</td>
                        <td>{{$list->created_at->format('d M Y | h:m A')}}</td>
                        <!-- <td>
                           <a href="{{url(env('ADMIN_DIR').'/testimonial/delete', $list->id)}}"  class="btn btn-danger btn-sm"
                            data-tr="tr_{{$list->id}}"
                            data-toggle="confirmation"
                            data-btn-ok-label="Delete" data-btn-ok-icon="fa fa-remove"
                            data-btn-ok-class="btn btn-sm btn-danger"
                            data-btn-cancel-label="Cancel"
                            data-btn-cancel-icon="fa fa-chevron-circle-left"
                            data-btn-cancel-class="btn btn-sm btn-default"
                            data-title="Are you sure you want to delete ?"
                            data-placement="left" data-singleton="true"
                            onclick="return confirm('Are you sure you want to delete this item?');">
                             Delete</a>
                         </td> -->
                        <!-- <td>
                          @if($list->status == 'disable')
                          <a href="{{ route('marchantchangestatus', [$list->id, 'field' => 'status', 'status' => 'enable', 'id' => $list->id]) }}" class="btn btn-success text-whites">Enable</a>
                          @else
                          <a href="{{ route('marchantchangestatus', [$list->id, 'field' => 'status', 'status' => 'disable', 'id' => $list->id]) }}" class="btn btn-danger text-white">Disable</a>
                          @endif
                        </td> -->
                        
                      </tr>

                      @endforeach
                    </tbody>
                  </table>

                </div>

              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>