<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Product;
use App\Model\ProductImage;
use App\Model\Shop;
use App\Model\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Product::with('category','shop')->orderBy('id', 'desc')
            ->paginate(10);
            foreach($lists as $l){
                $l->images = ProductImage::where('product_id',$l->id)->get();
            }
        // set page and title ------------------
        $page  = 'product.list';
        $title = 'Product List';
        $data  = compact('page', 'title', 'lists');
        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page  = "product.add";
        $title = "Product Add";

        $shop = Shop::get();
        $shopArr = [
            '' => 'Select Shop'
        ];
        foreach($shop as $c){
            $shopArr[$c->id] = $c->name;
        }
        $cat = Category::get();
        $catArr = [
            '' => 'Select Category'
        ];
        foreach($cat as $c){
            $catArr[$c->id] = $c->name;
        }
        $edit='';
        
        $data  = compact('page', 'title', 'request','shopArr', 'catArr', 'edit');
        return view('admin.layout',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = [
            'name'          => 'required',
            'shop_id'       => 'required',
            'category_id'   => 'required',
            'price'         => 'required',
            'image'         => 'required',
        ];
        
        
        $request->validate($rules);
        // $input = $request->all();
        
        
         
        // $input['password'] =  Hash::make($request->record['password']);
        $obj = new Product();  
        if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
            $optimizePath = public_path().'/imgs/product/';
            $filename = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$filename, 72);

			$obj->image = $filename;

		}  
        
        $obj->name              = $request->name;
        $obj->slug              = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug);
        $obj->description       = $request->description;
        $obj->category_id       = $request->category_id;
        $obj->shop_id           = $request->shop_id;
        $obj->material          = $request->material;
        $obj->product_weight    = $request->product_weight;
        $obj->diamond_weight    = $request->diamond_weight;
        $obj->size              = $request->size;
        $obj->price             = $request->price;
        // $obj->images = json_encode($data);
        
        $obj->save(); 

        if ($request->hasFile('images')) {

            $logo = $request->file('images');
            // dd($logo);
            
            foreach ($logo as $l) {
                
                $name = time().$l->getClientOriginalName();
                
                $image_resize = Image::make($l->getRealPath());
                $image_resize->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('imgs/product/' . $name));
                
                $imagedb = new ProductImage();
                $imagedb->status = 1;
                $imagedb->name = $name;
                
                $imagedb->product_id = $obj->id;
                
                $imagedb->save();
                
            }
            
            } 
            
        

        return redirect(route('product.index'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, Request $request)
    {
        $edit = Product::find($product->id);
        $request->replace($edit->toArray());       
        $request->flash();
        $shop = Shop::get();
        $shopArr = [
            '' => 'Select Shop'
        ];
        foreach($shop as $c){
            $shopArr[$c->id] = $c->name;
        }
        $cat = Category::get();
        $catArr = [
            '' => 'Select Category'
        ];
        foreach($cat as $c){
            $catArr[$c->id] = $c->name;
        }
        $productimage = ProductImage::where('product_id',$product->id)->get();
        $page  = 'product.edit';
        $title = 'Product Edit';
        $data  = compact('page', 'title','edit','request','catArr', 'shopArr','productimage');

        // return data to view
        return view('admin.layout', $data);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $rules = [
            'name'         => 'required',
            'shop_id'       => 'required',
            'category_id'        => 'required',
            'price'        => 'required',
            // 'image' => 'required',
        ];
        
        
        $request->validate($rules);
        $obj = Product::find($product->id);
        
        if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
            $optimizePath = public_path().'/imgs/product/';
            $filename = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$filename, 72);

			$obj->image = $filename;

		}  
        
        $obj->name              = $request->name;
        $obj->slug              = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug);
        $obj->description       = $request->description;
        $obj->category_id       = $request->category_id;
        $obj->shop_id           = $request->shop_id;
        $obj->price             = $request->price;
        $obj->material          = $request->material;
        $obj->product_weight    = $request->product_weight;
        $obj->diamond_weight    = $request->diamond_weight;
        $obj->size              = $request->size;
        // $obj->images = json_encode($data);
        
        
        if ($request->hasFile('images')) {

            $logo = $request->file('images');
            
            
            foreach ($logo as $l) {
                
                $name = time().$l->getClientOriginalName();
                
                $image_resize = Image::make($l->getRealPath());
                $image_resize->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('imgs/product/' . $name));
                
                $imagedb = new ProductImage();
                $imagedb->status = 1;
                $imagedb->name = $name;
                
                $imagedb->product_id = $product->id;
                
                $imagedb->save();
                
            }
            
            } 
            $obj->update(); 
        

        return redirect(route('product.index'))->with('success', 'Success! Updated Successfully.');

    }
    public function changeproductStatus(Request $request)
    {
        // dd($request->id);
        $user = ProductImage::find($request->id);
        $user->status = $request->status;
        $user->save();
  
        return response()->json(['success'=>'Status change successfully.']);
    }
    public function removeProductimage(Request $request)
    {
        // dd($request->id);
        $user = ProductImage::find($request->id);
        $user->delete();
        
  
        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        
        $product->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        
        $ids = $request->sub_chk;
        ProductImage::where('product_id',$ids)->delete();
        // dd($ids);
        Product::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }
    public function changestatus(Request $request, Product $user)
    {
        // dd($user);
        $user->status = $request->status;
        $user->save();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
