<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Enquery;
use App\Model\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB; 

class MarchantenqueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->firstOrFail();
        // $lists = Enquery::where('shop_id', $shop->id)->get();
        $lists = DB::table('enqueries')
						->select('enqueries.*','products.name as product_name','products.price as product_price','categories.name as category_name','products.image as products_image')
						->leftjoin('products','enqueries.product_id' ,'products.id')
						->leftjoin('categories','products.category_id' ,'categories.id')
                ->where('enqueries.shop_id', '=', $shop->id)
				->get();
        
        if ($lists->isEmpty()) {
            $re = [
                'status' => false,
                'message'    => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status'     => true,
                'message'    => $lists->count() . " records found.",
                'data'       => $lists
            ];
        }

        return response()->json($re);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Enquery $enquery)
    {
        $re = [
			'status' 	=> true,
			'data'   	=> $enquery
		];

		return response()->json($re);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
