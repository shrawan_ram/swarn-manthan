<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\City;
use App\Model\Enquery;
use Illuminate\Http\Request;
use Validator;

use App\Model\Notification;
use Illuminate\Support\Facades\DB;
use App\Model\Offer;
use App\Model\Product;
use App\Model\Shop;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
	public function index(Request $request)
	{
		$offer = Offer::latest()->get();
		$category = Category::latest()->get();
		$product = Product::latest()->with('shop','category')->get();
		
// 		$product = DB::table('products')
// 						->select('products.*','products.name as product_name','shops.*','categories.name AS cat_name','cities.name AS city_name','users.name AS user_name','users.mobile AS user_mobile','users.email AS user_email','shops.latitude','shops.longitude')
// 						->leftjoin('categories','products.category_id' ,'categories.id')
// 						->leftjoin('shops','products.shop_id' ,'shops.id')
// 						->leftjoin('cities','shops.city_id' ,'cities.id')
// 						->leftjoin('users','shops.user_id' ,'users.id')
//                         ->get();

		$homeArr = [];

		$homeArr[0] = [
        	'title'	=> "offers",
        	'data'	=> $offer
        ];
		$homeArr[1] = [
        	'title'	=> "category",
        	'data'	=> $category
        ];
		$homeArr[2] = [
        	'title'	=> "product",
        	'data'	=> $product
        ];

		$re = [
			'status' => true,
			'data'   => $homeArr
		];
		return response()->json($re);
	}

	public function city()
	{
		$lists = City::latest()->with('state')->get();

		$re = [
			'status' 	=> true,
			'data'   	=> $lists
		];

		return response()->json($re);
	}
}
