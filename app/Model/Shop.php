<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $guarded  = [];
    protected $with  = ['user','city'];
    
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
        
    }
    public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
        
    }
}
